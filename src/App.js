import React from 'react';
import Contact from "./components/Contact";

function App() {
  return (
    <div className="App">
      <h1>Contact React App</h1>
      <Contact />
    </div>
  );
}

export default App;
