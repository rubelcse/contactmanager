import React, { Component } from 'react'

export class Contact extends Component {
  render() {
    return (
      <div>
          <h3>Name : John Doe</h3>
          <ul>
            <li>Email : johndoe@gramil.com</li>
            <li>Contact No : +8804569865</li>
          </ul>
      </div>
    )
  }
}

export default Contact;
